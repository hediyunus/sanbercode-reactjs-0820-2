import React, { Component } from 'react'

class Timer extends Component {
  constructor(props) {
    super(props)
    var date = new Date()
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    var waktusekarang = hours + ':' + minutes + ':' + seconds + ' ' + ampm
    this.state = {
      time: 101,
      waktu: waktusekarang,
      Time: true
    }
  }

  componentDidMount() {
    if (this.props.start !== undefined) {
      this.setState({ time: this.props.start })
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }
  componentDidUpdate() {
    if (this.state.Time === true){
      if (this.state.time <= 0) {
        this.setState({ Time: false })
      }
    }

  

  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1
    });
  }


  render() {
    return (
      <>
        {this.state.Time && (
          <>
            <h1 style={{ textAlign: "center" }}>
              Sekarang jam: {this.state.waktu} <br />
        hitung mundur :  {this.state.time}
            </h1>
          </>
        )}
      </>
    )
  }
}

export default Timer
