
import React, { useContext } from "react"
import { ColorContext } from "./ColorContext"


const Button = () => {
    const light = '#FFFFFF';
    const dark = '#000000';

    const [color, setColor] = useContext(ColorContext)

    const ChangeColor = () => {
        const newColor = color.warna === dark ? light : dark;
        setColor({ warna: newColor })
    }
    return (
        <div style={{ width: '55%', textAlign: "center", border: '1px solid black', marginLeft: '320px', padding: "10px" }}>
            <button onClick={() => ChangeColor()}>Change Theme</button>

        </div>
    );
};

export default Button;
