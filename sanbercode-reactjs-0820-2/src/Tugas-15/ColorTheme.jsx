import React from "react"
import { ColorProvider } from "./ColorContext.jsx"
import Routes from "./router.jsx"

const Color = () => {
  return (
    <ColorProvider>
      <Routes />
    </ColorProvider>
  )
}

export default Color
