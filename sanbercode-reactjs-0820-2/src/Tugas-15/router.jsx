
import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Tugas9 from '../Tugas-9/tugas_9.jsx';
import Tugas10 from '../Tugas-10/tugas_10.jsx';
import Timer from '../Tugas-11/timer.jsx';
import BuahCRUD from '../Tugas-12/crud.jsx';
import DaftarBuah from '../Tugas-13/axios.jsx';
import Buah from '../Tugas-14/Buah.jsx';
import Button from '../Tugas-15/Color.jsx';
import Nav from './navigation.jsx';
// import {ColorProvider} from "./ColorContext.jsx"



const Routes = () => {

  return (
    <>
      <Router>
        <Nav />
        <Switch>
          <Route exact path="/">
            <Button />
          </Route>
          <Route path="/sembilan">
            <Tugas9 />
          </Route>
          <Route path="/sepuluh">
            <Tugas10 />
          </Route>
          <Route path="/sebelas">
            <Timer />
          </Route>
          <Route path="/duabelas">
            <BuahCRUD />
          </Route>
          <Route path="/tigabelas">
            <DaftarBuah />
          </Route>
          <Route path="/empatbelas">
            <Buah />
          </Route>
        </Switch>
      </Router>
    </>
  );
};

export default Routes;
