import React, { useState, createContext } from "react";

export const ColorContext = createContext();

export const ColorProvider = props => {
  const dark = '#000000';
  const [color, setColor] = useState({ warna: dark })


  return (

    <ColorContext.Provider value={[color, setColor]}>
      {props.children}
    </ColorContext.Provider>

  );

};
