
import React, { useContext } from "react"
import { ColorContext } from "./ColorContext"
import { Link } from "react-router-dom";


const Nav = () => {
  const [color] = useContext(ColorContext)
  return (
    <>

      <div>
        <nav style={{ display: "inline-block", backgroundColor: color.warna, marginLeft: "320px", marginBottom: "10px", textAlign: "center" }} >

          <ul>
            <Link to="/" style={{ marginRight: "12px" }}>Home</Link>
            <Link to="/sembilan" style={{ marginRight: "12px" }}>Pengenalan Reactjs</Link>
            <Link to="/sepuluh" style={{ marginRight: "12px" }}>Components and Props</Link>
            <Link to="/sebelas" style={{ marginRight: "12px" }}>Timer</Link>
            <Link to="/duabelas" style={{ marginRight: "12px" }}>Simple Hooks</Link>
            <Link to="/tigabelas" style={{ marginRight: "12px" }}>Hooks dan Axios</Link>
            <Link to="/empatbelas" style={{ marginRight: "12px" }}>Context</Link>

          </ul>
        </nav>
      </div>
    </>
  );
};

export default Nav;
