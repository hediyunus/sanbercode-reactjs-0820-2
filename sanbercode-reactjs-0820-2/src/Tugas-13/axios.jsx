import React, { useState, useEffect } from "react"
import axios from 'axios';

const DaftarBuah = () => {
  const [dataBuah, setdataBuah] = useState(null)
  const [input, setInput] = useState({ name: "", price: "", weight: 0, id: null })

  useEffect(() => {
    if (dataBuah === null) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
          setdataBuah(res.data)
        })
    }
  }, [dataBuah]);


  const submitForm = (event) => {
    event.preventDefault()

    if (input.id === null) {
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name: input.name, price: input.price, weight: input.weight })
        .then(res => {
          var data = res.data
          setdataBuah([...dataBuah, { id: data.id, name: data.name, price: data.price, weight: data.weight }])
          setInput({ id: null, name: "", price: "", weight: 0 })
        })

    } else {

      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, { name: input.name, price: input.price, weight: input.weight })
        .then(res => {
          var newdataBuah = dataBuah.map(x => {
            if (x.id === input.id) {
              x.name = input.name
              x.price = input.price
              x.weight = input.weight
            }
            return x
          })
          setdataBuah(newdataBuah)
          setInput({ id: null, name: "", price: "", weight: 0 })
        })
    }
  }

  const deleteBuah = (event) => {
    var idBuah = parseInt(event.target.value)
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
      .then(res => {
        var newdataBuah = dataBuah.filter(x => x.id !== idBuah)
        setdataBuah(newdataBuah)
      })
  }


  const handleChange = (event) => {
    let value = event.target.name
    switch (value) {
      case "name":
        {
          setInput({ ...input, name: event.target.value })
          break
        }
      case "price":
        {
          setInput({ ...input, price: event.target.value })
          break
        }
      case "weight":
        {
          setInput({ ...input, weight: event.target.value })
          break
        }
      default:
        { break; }
    }
  }
  //   const changeInputPrice = (event) =>{
  //     var value= event.target.value
  //     setInput({...input, price: value})
  //   }
  //   const changeInputWeight = (event) =>{
  //     var value= event.target.value
  //     setInput({...input, weight: value})
  //   }

  const editForm = (event) => {
    var idBuah = parseInt(event.target.value)
    var Buah = dataBuah.find(x => x.id === idBuah)

    setInput({ id: idBuah, name: Buah.name, price: Buah.price, weight: Buah.weight })

  }

  return (
    <>
      <div style={{ width: "70vw", margin: "0 auto" }}>
        <h1 style={{ textAlign: "center" }}>Daftar Harga Buah</h1>
        <table id='daftar'>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>

              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {
              dataBuah !== null && dataBuah.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.weight}</td>

                    <td>
                      <button value={item.id} style={{ marginRight: "5px" }} onClick={editForm}>Edit</button>
                      <button value={item.id} onClick={deleteBuah}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        <br />
        <br />
        {/* Form */}
        <h1 style={{ textAlign: "center" }}>Form Daftar Harga Buah</h1>
        <div style={{ width: "50%", margin: "0 auto", display: "block" }}>
          <div style={{ border: "1px solid #aaa", padding: "20px" }}>
            <form onSubmit={submitForm}>
              <label style={{ float: "left" }}>
                Nama:
              </label>
              <input style={{ float: "right" }} type="text" name="name" value={input.name} onChange={handleChange} required />
              <br />
              <br />
              <label style={{ float: "left" }}>
                Harga:
              </label>
              <input style={{ float: "right" }} type="text" name="price" value={input.price} onChange={handleChange} required />
              <br />
              <br />
              <label style={{ float: "left" }}>
                Berat (dalam gram):
              </label>
              <input style={{ float: "right" }} type="number" name="weight" value={input.weight} onChange={handleChange} required />
              <br />
              <br />
              <div style={{ width: "100%", paddingBottom: "20px" }}>
                <button style={{ float: "right" }}>Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  )
}

export default DaftarBuah
