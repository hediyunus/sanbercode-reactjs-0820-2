import React from 'react';

const data = [
  {name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"},
  {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"},
   {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"},
   {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
 ]

 class Gambar extends React.Component {
   render() {
     return <h1>{this.props.gambar}</h1>;
   }
 }

 class Nama extends React.Component {
   render() {
     return <h1>{this.props.name} {this.props.nama}</h1>;
   }
 }
 class Profesi extends React.Component {
   render() {
     return <h1>{this.props.prof}</h1>;
   }
 }
 class Age extends React.Component {
   render() {
     return <h1>{this.props.umur}</h1>;
   }
 }
 export default class UserInfo extends React.Component {
   renderTableData() {
      return data.map((daftar, index) => {
         const { name, age,  gender, profession, photo} = daftar
         return (
           <div style={{border: "1px solid #000"}}>
              <Gambar gambar=<img src={photo} alt="gambar"  style={{ width : '270px', display: 'block'}}/>/>
              <Nama name={gender === "Male" ? "Mr." : (gender === "Female" ? "Mrs." : "Undefined")} nama={name}/>
              <Profesi prof={profession}/>
              <Age umur={age} years old/>
            </div>
         )
      })
   }
   render(){
     return (
       <>
       <div style={{ width : '20%', textAlign:"center",  marginLeft : '400px' }}>
                  {this.renderTableData()}
         </div>
      </>
     )
   }
}
