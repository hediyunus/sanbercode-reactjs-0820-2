import React, { useContext } from "react"
import { BuahContext } from "./BuahContext"
import axios from 'axios';

const BuahList = () => {
  // const [ , setInput] = useState({name: "", price: "", weight: 0, id: null})
  const [dataBuah, setdataBuah, input, setInput] = useContext(BuahContext)
  const editForm = (event) => {
    var idBuah = parseInt(event.target.value)
    var Buah = dataBuah.find(x => x.id === idBuah)
    setInput({ ...input, id: idBuah, name: Buah.name, price: Buah.price, weight: Buah.weight })

  }

  const deleteBuah = (event) => {
    var idBuah = parseInt(event.target.value)
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
      .then(res => {
        var newdataBuah = dataBuah.filter(x => x.id !== idBuah)
        setdataBuah(newdataBuah)
      })
  }

  return (
    <>

      <div style={{ width: "70vw", margin: "0 auto" }}>
        <h1 style={{ textAlign: "center" }}>Daftar Harga Buah</h1>
        <table id='daftar'>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat (Kg)</th>

              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {
              dataBuah !== null && dataBuah.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.weight / 1000}</td>
                    <td>
                      <button value={item.id} style={{ marginRight: "5px" }} onClick={editForm}>Edit</button>
                      <button value={item.id} onClick={deleteBuah}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        <br />
        <br />
        {/* Form */}
      </div>
    </>
  )

}

export default BuahList
