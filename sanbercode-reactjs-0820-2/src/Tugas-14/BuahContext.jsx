import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const BuahContext = createContext();

export const BuahProvider = props => {
  const [dataBuah, setdataBuah] = useState(null)
  const [input, setInput] = useState({name: "", price: "", weight: 0, id: null})

  useEffect(() => {
    if (dataBuah === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        setdataBuah(res.data)
      })
    }
  }, [dataBuah]);
  return (
    <BuahContext.Provider value={[dataBuah, setdataBuah, input, setInput]}>
      {props.children}
    </BuahContext.Provider>
  );
};
