import React, {useContext,  createContext} from "react"
import {BuahContext} from "./BuahContext"
import axios from 'axios';
export const EditContext = createContext();

const BuahForm = () =>{
  // const [input, setInput] = useState({name: "", price: "", weight: 0, id: null})
  const [dataBuah, setdataBuah, input, setInput] = useContext(BuahContext)

 
  const submitForm = (event) =>{
    event.preventDefault()
    
    if ( input.id === null){
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name: input.name, price:input.price, weight: input.weight})
      .then(res => {
        var data = res.data
        setdataBuah([...dataBuah, {id: data.id, name: data.name, price: data.price, weight: data.weight}])
        setInput({id: null, name: "", price: "", weight: 0})
      })

    }else{

      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, { name: input.name, price:input.price, weight: input.weight})
      .then(res => {
        var newdataBuah = dataBuah.map(x => {
          if (x.id === input.id){
            x.name = input.name
            x.price = input.price
            x.weight = input.weight
          }
          return x
        })
        setdataBuah(newdataBuah)
        setInput({id: null, name: "", price: "", weight: 0})
      })
    }
  }


  const handleChange = (event) =>{
    let value = event.target.name
    switch (value){
      case "name":
      {
        setInput({...input, name: event.target.value})
        break
      }
      case "price":
      {
        setInput({...input, price: event.target.value})
        break
      }
      case "weight":
      {
        setInput({...input, weight: event.target.value})
          break
      }
    default:
      {break;}
    }
  }

  return(
    <>
      <div style={{width: "70vw", margin: "0 auto"}}>
        {/* Form */}
        <h1 style={{textAlign: "center"}}>Form Daftar Harga Buah</h1>
        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
          <div style={{border: "1px solid #aaa", padding: "20px"}}>
            <form onSubmit={submitForm}>
              <label style={{float: "left"}}>
                Nama:
              </label>
              <input style={{float: "right"}} type="text" name="name" value={input.name} onChange={handleChange} required/>
              <br/>
              <br/>
              <label style={{float: "left"}}>
                Harga:
              </label>
              <input style={{float: "right"}} type="text" name="price" value={input.price} onChange={handleChange} required/>
              <br/>
              <br/>
              <label style={{float: "left"}}>
                Berat (dalam gram):
              </label>
              <input style={{float: "right"}} type="number" name="weight" value={input.weight} onChange={handleChange} required/>
              <br/>
              <br/>
              <div style={{width: "100%", paddingBottom: "20px"}}>
                <button style={{ float: "right"}}>Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  )

}

export default BuahForm
