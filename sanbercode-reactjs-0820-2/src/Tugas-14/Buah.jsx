import React from "react"
import {BuahProvider} from "./BuahContext.jsx"
import BuahList from "./BuahList.jsx"
import BuahForm from "./BuahForm.jsx"

const Buah = () =>{
  return(
    <BuahProvider>
      <BuahList/>
      <BuahForm/>
    </BuahProvider>
  )
}

export default Buah
