import React from 'react';
let dataHargaBuah = [
   { nama: "Semangka", harga: 10000, berat: 1000 },
   { nama: "Anggur", harga: 40000, berat: 500 },
   { nama: "Strawberry", harga: 30000, berat: 400 },
   { nama: "Jeruk", harga: 30000, berat: 1000 },
   { nama: "Mangga", harga: 30000, berat: 500 }
]


export default class Tugas10 extends React.Component {
   render() {
      return (
         <div style={{ width: '55%', textAlign: "center", marginLeft: '330px' }}>
            <h1 id='title'>Tabel Harga Buah</h1>

            <table id='daftar'>
               <thead>
                  <tr>
                     <th>Nama</th>
                     <th>Harga</th>
                     <th>Berat</th>
                  </tr>
               </thead>
               <tbody>
                  {
                     dataHargaBuah.map((item, index) => {
                        return (
                           <tr key={index}>
                              <td><b>{item.nama}</b></td>
                              <td><b>{item.harga}</b></td>
                              <td><b>{item.berat / 1000} Kg</b></td>
                           </tr>
                        )
                     })
                  }
               </tbody>
            </table>
         </div>
      )
   }
}
