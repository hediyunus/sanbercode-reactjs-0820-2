import React from 'react';
export default class Tugas9 extends React.Component {
  render() {
    return (
      <div style={{ width: '55%', textAlign: "center", border: '1px solid black', marginLeft: '330px' }}>
        <h1 style={{ textAlign: "center" }}><strong>Form Pembelian Buah</strong> </h1>
        <table style={{ textAlign: "left", marginLeft: '10px' }}>
          <thead>
            <tr>

              <th><label htmlFor="nama"><strong> Nama Pelanggan </strong></label></th>
              <th style={{ padding: '0px 0px 0px 17px' }}><input type="text" id="nama" name="nama"></input></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th><br /><br /><br /><label> Daftar Item</label></th>
              <th style={{ padding: '0px 0px 0px 15px' }}>
                <input type="checkbox" id="Semangka" name="buah" value="Semangka"></input>
                <label htmlFor="Semangka">Semangka</label> <br />
                <input type="checkbox" id="Jeruk" name="buah" value="Jeruk"></input>
                <label htmlFor="Jeruk">Jeruk</label> <br />
                <input type="checkbox" id="Nanas" name="buah" value="Nanas"></input>
                <label htmlFor="Nanas">Nanas</label> <br />
                <input type="checkbox" id="Salak" name="buah" value="Salak"></input>
                <label htmlFor="Salak">Salak</label> <br />
                <input type="checkbox" id="Anggur" name="buah" value="Anggur"></input>
                <label htmlFor="Anggur">Anggur</label>
              </th>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th><input type="submit" value="Kirim"></input></th>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}
